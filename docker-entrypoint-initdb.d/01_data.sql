INSERT INTO users(id,login, password)
VALUES (1,'liza', '{argon2}$argon2id$v=19$m=4096,t=3,p=1$VD2gbE9s9SvxSU3QnmeO9w$hosiwDgCWLdyZ6xrysnDg9fDE38frM65jxOj58ZkCXs'),
       (2,'masha', '{argon2}$argon2id$v=19$m=4096,t=3,p=1$VD2gbE9s9SvxSU3QnmeO9w$hosiwDgCWLdyZ6xrysnDg9fDE38frM65jxOj58ZkCXs'),
       (3,'support', '{argon2}$argon2id$v=19$m=4096,t=3,p=1$VD2gbE9s9SvxSU3QnmeO9w$hosiwDgCWLdyZ6xrysnDg9fDE38frM65jxOj58ZkCXs');

INSERT INTO user_authorities(user_id,authority)
VALUES (1,'ROLE_USER'),
       (2,'ROLE_USER'),
       (3,'ROLE_SUPPORT');

-- DROP table authors CASCADE;
--
-- CREATE VIEW authors AS
-- SELECT id,login
-- FROM users;
