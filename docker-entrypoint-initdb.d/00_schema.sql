CREATE TABLE users
(
    id      BIGSERIAL PRIMARY KEY,
    login    TEXT   NOT NULL UNIQUE,
    password TEXT   NOT NULL
);

CREATE TABLE user_authorities
(
    user_id  BIGINT REFERENCES users(id),
    authority TEXT NOT NULL
);

CREATE TABLE tickets
(
    id      BIGSERIAL PRIMARY KEY,
    name    TEXT NOT NULL,
    content TEXT NOT NULL,
    status  TEXT DEFAULT 'OPEN',
    author_id BIGINT REFERENCES users
);

CREATE TABLE comments
(
    id      BIGSERIAL PRIMARY KEY,
    ticket_id  BIGINT REFERENCES tickets,
    text TEXT NOT NULL,
    author_id BIGINT REFERENCES users
);

CREATE VIEW authors AS
SELECT id,login
FROM users;


