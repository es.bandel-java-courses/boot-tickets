package com.example.springbootsecurityapp.entity;


import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@Table(name = "users")
@Entity
public class UserEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @Column(nullable = false, updatable = false, unique = true, columnDefinition = "TEXT")
  private String login;
  @Column(nullable = false, columnDefinition = "TEXT")
  private String password;
  @ElementCollection
  @CollectionTable(
          name="user_authorities",
          joinColumns=@JoinColumn(name="user_id")
  )
  @Column(name="authority",columnDefinition = "TEXT")
  private Collection<String> authorities;
}
