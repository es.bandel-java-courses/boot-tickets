package com.example.springbootsecurityapp.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@Table(name = "authors")
@Immutable
@Entity
public class AuthorEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @Column(updatable = false, nullable = false, columnDefinition = "TEXT")
  private String login;

  public AuthorEntity(final long id) {
    this.id = id;
  }
}
