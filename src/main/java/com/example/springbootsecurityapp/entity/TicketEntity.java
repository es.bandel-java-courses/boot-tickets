package com.example.springbootsecurityapp.entity;


import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@Table(name = "tickets")
@Entity
public class TicketEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @Column(nullable = false, columnDefinition = "TEXT")
  private String name;
  @Column(nullable = false, columnDefinition = "TEXT")
  private String content;
  @Column( columnDefinition = "TEXT")
  private String status="OPEN";
  @ManyToOne(fetch = FetchType.LAZY)
  private AuthorEntity author;

  public TicketEntity(final long id) {
    this.id = id;
  }
}
