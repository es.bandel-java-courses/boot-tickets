package com.example.springbootsecurityapp.entity;


import lombok.*;
import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@Getter
@Setter
@Table(name = "comments")
@Immutable
@Entity
public class CommentEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @ManyToOne(fetch = FetchType.LAZY)
  private TicketEntity ticket;
  @Column(nullable = false, columnDefinition = "TEXT")
  private String text;
  @ManyToOne(fetch = FetchType.LAZY)
  private AuthorEntity author;

  public CommentEntity(final long id) {
    this.id = id;
  }
}
