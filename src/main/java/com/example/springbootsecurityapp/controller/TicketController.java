package com.example.springbootsecurityapp.controller;

import com.example.springbootsecurityapp.dto.TicketClosedRS;
import com.example.springbootsecurityapp.dto.TicketRQ;
import com.example.springbootsecurityapp.dto.TicketRS;
import com.example.springbootsecurityapp.service.TicketService;
import com.example.springbootsecurityapp.support.security.ApplicationUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
@Validated
@RequestMapping("/tickets")
@RestController
@RequiredArgsConstructor
public class TicketController {
    private final TicketService ticketService;

    @PostMapping("/create")
    @PreAuthorize("hasRole('USER')")
    public TicketRS save(@AuthenticationPrincipal final ApplicationUserDetails userDetails,@Valid @RequestBody final TicketRQ request) {
        return ticketService.save(userDetails, request);
    }

    @GetMapping("/show")
    @PreAuthorize("hasAnyRole('USER','SUPPORT')")
    public List<TicketRS> getTickets(
            @AuthenticationPrincipal final ApplicationUserDetails userDetails,
            @RequestParam(defaultValue = "0") final long offset,
            @RequestParam(defaultValue = "50") final int limit,
            @RequestParam(defaultValue = "0") int page
    ) {
        return ticketService.getAll(userDetails, offset, limit,page);
    }

    @PutMapping("/close/{id}")
    @PreAuthorize("hasRole('USER')")
    public TicketClosedRS update(@AuthenticationPrincipal final ApplicationUserDetails userDetails, @PathVariable long id) {
        return ticketService.close(userDetails, id);
    }

}
