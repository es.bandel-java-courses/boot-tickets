package com.example.springbootsecurityapp.controller;


import com.example.springbootsecurityapp.dto.UserRS;
import com.example.springbootsecurityapp.support.security.ApplicationUserDetails;
import com.example.springbootsecurityapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Validated
@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {
  private final UserService service;

  @GetMapping
@PreAuthorize("isAuthenticated()")
  public List<UserRS> getAll(
      @AuthenticationPrincipal ApplicationUserDetails principal,
      @RequestParam(defaultValue = "0") int page
  ) {
    return service.getAll(page);
  }
}
