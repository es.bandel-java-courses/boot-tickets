package com.example.springbootsecurityapp.controller;

import com.example.springbootsecurityapp.dto.*;
import com.example.springbootsecurityapp.service.CommentService;
import com.example.springbootsecurityapp.support.security.ApplicationUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RequestMapping("/comments")
@RestController
@RequiredArgsConstructor
public class CommentController {
    private final CommentService commentService;

    @PostMapping("/create/{id}")
    @PreAuthorize("hasAnyRole('USER','SUPPORT')")
    public CommentRS save(@AuthenticationPrincipal final ApplicationUserDetails userDetails, @Valid @RequestBody final CommentRQ request, @PathVariable long id) {
        return commentService.save(userDetails, request,id);
    }

    @GetMapping("/show/{id}")
    @PreAuthorize("hasAnyRole('USER','SUPPORT')")
    public List<CommentRS> getComments(
            @AuthenticationPrincipal final ApplicationUserDetails userDetails,
            @PathVariable long id,
            @RequestParam(defaultValue = "0") final long offset,
            @RequestParam(defaultValue = "50") final int limit
    ) {
        return commentService.getAllCommentsByTicketId(userDetails, id, offset, limit);
    }


}
