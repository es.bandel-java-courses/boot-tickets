package com.example.springbootsecurityapp.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TicketRQ {
    @NotNull
    @Size(min=1, max=20)
    private String name;
    @NotNull
    @Size(min=1, max=1000)
    private String content;
}
