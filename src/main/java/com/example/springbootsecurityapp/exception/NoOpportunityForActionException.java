package com.example.springbootsecurityapp.exception;


public class NoOpportunityForActionException extends RuntimeException{
    public NoOpportunityForActionException() {
    }

    public NoOpportunityForActionException(String message) {
        super(message);
    }

    public NoOpportunityForActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoOpportunityForActionException(Throwable cause) {
        super(cause);
    }

    public NoOpportunityForActionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
