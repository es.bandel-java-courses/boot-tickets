package com.example.springbootsecurityapp.support.error.advice;

import com.example.springbootsecurityapp.exception.ItemNotFoundException;
import com.example.springbootsecurityapp.exception.NoOpportunityForActionException;
import com.example.springbootsecurityapp.exception.TickerIsClosedException;
import com.example.springbootsecurityapp.support.error.dto.ErrorRS;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;

@RestControllerAdvice
public class GlobalControllerAdvice {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorRS exception(final MethodArgumentNotValidException e) {
        return new ErrorRS(ErrorRS.VALIDATION_ERROR);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorRS exception(final NoOpportunityForActionException e) {
        return new ErrorRS(ErrorRS.FORBIDDDEN_ACTION);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorRS exception(final Throwable e) {
        return new ErrorRS(ErrorRS.UNKNOWN_ERROR);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorRS exception(final ItemNotFoundException e) {
        return new ErrorRS(ErrorRS.ELEMENT_NOT_FOUND);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorRS exception(final TickerIsClosedException e) {
        return new ErrorRS(ErrorRS.ELEMENT_NOT_FOUND);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorRS exception(final EntityNotFoundException e) {
        return new ErrorRS(ErrorRS.ELEMENT_NOT_FOUND);
    }


}
