package com.example.springbootsecurityapp.support.error.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ErrorRS {
 public static final String FORBIDDDEN_ACTION = "err.forbidden.action";
  public static final String UNKNOWN_ERROR = "err.internal.unknown";
  public static final String VALIDATION_ERROR = "err.validation.invalid";
    public static final String ELEMENT_NOT_FOUND = "err.element.not.found";

    private String code;
}
