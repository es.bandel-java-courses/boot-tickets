package com.example.springbootsecurityapp.service;

import com.example.springbootsecurityapp.dto.UserRS;
import com.example.springbootsecurityapp.entity.UserEntity;
import com.example.springbootsecurityapp.mapper.UserEntityMapper;
import com.example.springbootsecurityapp.repository.UserRepository;
import com.example.springbootsecurityapp.support.security.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;


@Transactional
@Service
@RequiredArgsConstructor
public class UserService implements InitializingBean, UserDetailsService {
  public static final int PAGE_SIZE = 50;
  public static final String SORT_PROPERTY = "id";
  private final UserRepository userRepository;
  private final UserEntityMapper userEntityMapper;

  private final PasswordEncoder passwordEncoder;

  private UserEntity notFoundUserEntity;

  @Override
  public void afterPropertiesSet() throws Exception {
    notFoundUserEntity = new UserEntity(-1, "notfound", passwordEncoder.encode("dummy password"), Collections.emptySet());
  }

  public List<UserRS> getAll(final int page) {
    final Optional<ApplicationUserDetails> principal = SecurityContextUtils.principal();
    final Page<UserEntity> entities = userRepository.findAll(PageRequest.of(page, PAGE_SIZE, Sort.Direction.ASC, SORT_PROPERTY));
    return userEntityMapper.toRS(entities.toList());
  }

  @Override
  public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
    final UserEntity saved = userRepository.findByLogin(username).orElseThrow(() -> new UsernameNotFoundException(username));
    return userEntityMapper.toUserDetails(saved);
  }




}
