package com.example.springbootsecurityapp.service;


import com.example.springbootsecurityapp.dto.TicketClosedRS;
import com.example.springbootsecurityapp.dto.TicketRQ;
import com.example.springbootsecurityapp.dto.TicketRS;
import com.example.springbootsecurityapp.entity.TicketEntity;
import com.example.springbootsecurityapp.exception.ItemNotFoundException;
import com.example.springbootsecurityapp.exception.NoOpportunityForActionException;
import com.example.springbootsecurityapp.mapper.TicketEntityMapper;
import com.example.springbootsecurityapp.repository.TicketRepository;
import com.example.springbootsecurityapp.support.security.ApplicationUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

import java.util.List;

@Transactional
@Service
@RequiredArgsConstructor
public class TicketService {
    private final TicketRepository ticketRepository;
    private final TicketEntityMapper ticketEntityMapper;
    private final String NEW_STATUS = "CLOSED";
    public static final int PAGE_SIZE = 50;
    public static final String SORT_PROPERTY = "id";

    public TicketRS save(final ApplicationUserDetails userDetails, final TicketRQ request) {
        final TicketEntity entity = ticketEntityMapper.fromTicketRQ(request, userDetails.getId());
        final TicketEntity saved = ticketRepository.save(entity);
        return ticketEntityMapper.toTicketRS(saved);
    }

    @Transactional(readOnly = true)
    public List<TicketRS> getAll(final ApplicationUserDetails userDetails, final long offset, final int limit, final int page) {
        if (userDetails.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_USER"))) {
            final List<TicketEntity> found = ticketRepository.findByUserId(userDetails.getId(), offset, limit);
            return ticketEntityMapper.toListOfTicketRS(found);
        } else {
            final Page<TicketEntity> found = ticketRepository.findAll(PageRequest.of(page, PAGE_SIZE, Sort.Direction.ASC, SORT_PROPERTY));
            return ticketEntityMapper.toListOfTicketRS(found.toList());
        }

    }

    public TicketClosedRS close(final ApplicationUserDetails userDetails, final long id) {
        final TicketEntity entity = ticketRepository.getById(id).orElseThrow(() -> new ItemNotFoundException("There are no tickets with id: "+id));
        if (userDetails.getId() == (entity.getAuthor().getId())) {
            ticketRepository.update(NEW_STATUS, id);
            return ticketEntityMapper.toTicketClosedRS(entity, NEW_STATUS);
        } else {
            throw new NoOpportunityForActionException("Only author can close ticket");
        }
    }

}
