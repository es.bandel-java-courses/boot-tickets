package com.example.springbootsecurityapp.service;


import com.example.springbootsecurityapp.dto.*;
import com.example.springbootsecurityapp.entity.CommentEntity;
import com.example.springbootsecurityapp.entity.TicketEntity;
import com.example.springbootsecurityapp.exception.ItemNotFoundException;
import com.example.springbootsecurityapp.exception.NoOpportunityForActionException;
import com.example.springbootsecurityapp.exception.TickerIsClosedException;
import com.example.springbootsecurityapp.mapper.CommentEntityMapper;
import com.example.springbootsecurityapp.mapper.TicketEntityMapper;
import com.example.springbootsecurityapp.repository.CommentRepository;
import com.example.springbootsecurityapp.repository.TicketRepository;
import com.example.springbootsecurityapp.support.security.ApplicationUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;
    private final TicketRepository ticketRepository;
    private final CommentEntityMapper commentEntityMapper;
    private final String NEW_STATUS = "CLOSED";
    private final String ROLE_SUPPORT = "ROLE_SUPPORT";


    public CommentRS save(final ApplicationUserDetails userDetails, final CommentRQ request, final long id) {
        final TicketEntity ticketEntity = ticketRepository.getById(id).orElseThrow(() -> new ItemNotFoundException("There are no tickets with id: "+id));
        if (ticketEntity.getStatus().equals(NEW_STATUS)){
            throw new TickerIsClosedException("Not possible to write comments here");
        }
        if(ticketEntity.getAuthor().getLogin().equals(userDetails.getUsername())||
                userDetails.getAuthorities().contains(new SimpleGrantedAuthority(ROLE_SUPPORT))){
            final CommentEntity entity = commentEntityMapper.fromCommentRQ(request, userDetails.getId(),id);
            final CommentEntity saved = commentRepository.save(entity);
            return commentEntityMapper.toCommentRS(saved);
        }
        else{
            throw new NoOpportunityForActionException("Only author and support can comment this ticket");
        }
    }

    @Transactional(readOnly = true)
    public List<CommentRS> getAllCommentsByTicketId(final ApplicationUserDetails userDetails, final long id,final long offset, final int limit) {
        final TicketEntity ticketEntity = ticketRepository.getById(id).orElseThrow(() -> new ItemNotFoundException("There are no tickets with id: "+id));
        if(ticketEntity.getAuthor().getLogin().equals(userDetails.getUsername())||
                userDetails.getAuthorities().contains(new SimpleGrantedAuthority(ROLE_SUPPORT))){
            final List<CommentEntity> comments;
            comments = commentRepository.findByTicketId(id,offset,limit);
            if (!comments.isEmpty()) {
                List<CommentRS> commentsOfCurrentTicket = comments.
                        stream().map(o -> commentEntityMapper.toCommentRS(o))
                        .collect(Collectors.toList());
                return commentsOfCurrentTicket;
            } else {
                throw new ItemNotFoundException("There are no comments of this ticket");
            }
        }
        else{
            throw new NoOpportunityForActionException("Only author and support can see comments of this ticket");
        }
    }


}
