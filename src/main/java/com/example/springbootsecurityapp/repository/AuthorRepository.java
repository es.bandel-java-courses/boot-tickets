package com.example.springbootsecurityapp.repository;

import com.example.springbootsecurityapp.entity.AuthorEntity;

import java.util.Optional;

public interface AuthorRepository extends JpaReadOnlyRepository<AuthorEntity, Long> {
  Optional<AuthorEntity> findByLogin(final String login);
}

