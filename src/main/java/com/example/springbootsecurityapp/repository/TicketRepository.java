package com.example.springbootsecurityapp.repository;


import com.example.springbootsecurityapp.entity.TicketEntity;
import com.example.springbootsecurityapp.entity.UserEntity;
import com.example.springbootsecurityapp.support.security.ApplicationUserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
public interface TicketRepository extends JpaRepository<TicketEntity, Long> {
  @Query(value = "SELECT * FROM tickets t WHERE t.author_id=?1 offset ?2 limit ?3", nativeQuery = true)
  List<TicketEntity> findByUserId(final long authorId, final long offset, final int limit);
  @Modifying
  @Query(value = "UPDATE tickets t SET status=?1 WHERE t.id=?2", nativeQuery = true)
  void update(final String status, final long id);

  Optional<TicketEntity> getById(final long id);
}
