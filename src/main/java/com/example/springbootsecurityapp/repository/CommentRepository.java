package com.example.springbootsecurityapp.repository;


import com.example.springbootsecurityapp.entity.CommentEntity;
import com.example.springbootsecurityapp.entity.TicketEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CommentRepository extends JpaRepository<CommentEntity, Long> {
    @Query(value = "SELECT * FROM comments c WHERE c.ticket_id=?1 offset ?2 limit ?3", nativeQuery = true)
    List<CommentEntity> findByTicketId(final long authorId, final long offset, final int limit);
}
