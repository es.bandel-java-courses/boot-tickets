package com.example.springbootsecurityapp.repository;

import com.example.springbootsecurityapp.entity.AuthorEntity;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;
@NoRepositoryBean
public interface JpaReadOnlyRepository<T, ID> extends Repository<T, ID> {
  AuthorEntity getById(ID id);
  Optional<T> findById(ID id);

  List<T> findAll();

  List<T> findAll(Sort sort);

  List<T> findAllById(Iterable<ID> ids);

  <S extends T> List<S> findAll(Example<S> example);

  <S extends T> List<S> findAll(Example<S> example, Sort sort);

  boolean existsById(ID id);

  long count();
}
