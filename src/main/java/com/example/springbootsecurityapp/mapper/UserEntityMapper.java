package com.example.springbootsecurityapp.mapper;

import com.example.springbootsecurityapp.dto.UserRS;
import com.example.springbootsecurityapp.entity.UserEntity;
import com.example.springbootsecurityapp.support.security.ApplicationUserDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Mapper
public interface UserEntityMapper {
  List<UserRS> toRS(final List<UserEntity> entities);

  @Mapping(target = "username", source = "login")
  @Mapping(target = "accountNonLocked", ignore = true)
  @Mapping(target = "accountNonExpired", ignore = true)
  @Mapping(target = "credentialsNonExpired", ignore = true)
  @Mapping(target = "enabled", ignore = true)
  ApplicationUserDetails toUserDetails(final UserEntity saved);

  default Collection<? extends GrantedAuthority> authoritiesFromText(final Collection<String> authorities){
    return authorities.stream()
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toSet());
  }
}
