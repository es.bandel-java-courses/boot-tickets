package com.example.springbootsecurityapp.mapper;


import com.example.springbootsecurityapp.dto.*;
import com.example.springbootsecurityapp.entity.AuthorEntity;
import com.example.springbootsecurityapp.entity.CommentEntity;
import com.example.springbootsecurityapp.entity.TicketEntity;
import com.example.springbootsecurityapp.exception.ItemNotFoundException;
import com.example.springbootsecurityapp.repository.AuthorRepository;
import com.example.springbootsecurityapp.repository.TicketRepository;
import lombok.Setter;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;


@Mapper
public abstract class CommentEntityMapper {

    @Setter(onMethod_ = @Autowired)
    private AuthorRepository authorRepository;
    @Setter(onMethod_ = @Autowired)
    private TicketRepository ticketRepository;


    @Mapping(target = "author", source = "authorId")
    @Mapping(target = "ticket", source = "ticketId")
    public abstract CommentEntity fromCommentRQ(
            final CommentRQ request,
            final long authorId,
            final long ticketId
    );

    public TicketEntity ticketFromId(final long id) {
        final TicketEntity ticketEntity = ticketRepository.getById(id).orElseThrow(() -> new ItemNotFoundException("There are no tickets with id: " + id));
        return ticketEntity;
    }

    public AuthorEntity authorFromId(final long id) {
        return authorRepository.getById(id);
    }

    @Mapping(target = "userLogin", source = "entity.author.login")
    @Mapping(target = "ticketId", source = "entity.ticket.id")
    public abstract CommentRS toCommentRS(final CommentEntity entity);

}
