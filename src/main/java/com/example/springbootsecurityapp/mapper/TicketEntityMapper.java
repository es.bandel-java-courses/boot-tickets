package com.example.springbootsecurityapp.mapper;


import com.example.springbootsecurityapp.dto.TicketClosedRS;
import com.example.springbootsecurityapp.dto.TicketRQ;
import com.example.springbootsecurityapp.dto.TicketRS;
import com.example.springbootsecurityapp.entity.AuthorEntity;
import com.example.springbootsecurityapp.entity.TicketEntity;
import com.example.springbootsecurityapp.repository.AuthorRepository;
import lombok.Setter;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@Mapper
public abstract class TicketEntityMapper {

  @Setter(onMethod_ = @Autowired)
  private AuthorRepository authorRepository;

  public abstract List<TicketRS> toListOfTicketRS(final List<TicketEntity> entities);

  @Mapping(target = "author", source = "authorId")
  public abstract TicketEntity fromTicketRQ(
      final TicketRQ request,
      final long authorId
  );

  public AuthorEntity authorFromId(final long id) {
    return authorRepository.getById(id);
  }

  @Mapping(target = "userLogin", source = "entity.author.login")
 public abstract TicketRS toTicketRS(final TicketEntity entity);

  @Mapping(target = "status", source = "newStatus")
    public abstract TicketClosedRS toTicketClosedRS(TicketEntity entity, String newStatus);
}
