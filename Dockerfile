FROM maven:3-openjdk-8-slim AS build
WORKDIR /app/build
COPY . .
RUN mvn -B package

FROM openjdk:8-slim
WORKDIR /app/bin
COPY --from=build /app/build/target/api.jar .
CMD ["java", "-jar", "api.jar"]